﻿using DC4B.FilmsBrowser.Common.ApiVM.Dashboard;
using DC4B.FilmsBrowser.Common.ApiVM.InitialView;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public interface IInitialViewService
    {
        DashboardVM GetInitialViewData(int userId);
        void UpdateUser(UserVM userVM);
    }
}
