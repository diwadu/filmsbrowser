﻿using System.Collections.Generic;
using DC4B.FilmsBrowser.Entities;

namespace DC4B.FilmsBrowser.Services.Abstract
{
    public interface ICategoryService : IBaseService<Category>
    {
       //custom logic here for eg checking permissions or something like this
        IEnumerable<Film> GetFilms(int categoryId);
    }
}
