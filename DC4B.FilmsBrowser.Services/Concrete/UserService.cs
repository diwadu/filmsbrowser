﻿using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.DAL.SqlServer;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC4B.FilmsBrowser.Services.Concrete
{
    public class UserService : BaseCRUDService<User>, IUserService
    {
        public UserService(ISqlUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
    }
}
