﻿using System.Linq;
using AutoMapper;
using DC4B.FilmsBrowser.Common.ApiVM.Dashboard;
using DC4B.FilmsBrowser.Common.ApiVM.InitialView;
using DC4B.FilmsBrowser.DAL;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using DC4B.FilmsBrowser.DAL.SqlServer;

namespace DC4B.FilmsBrowser.Services.Concrete
{
    public class DashboardService : IDashboardService
    {
        private readonly ISqlUnitOfWork _unitOfWork;

        public DashboardService(ISqlUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public DashboardVM GetData(int userId)
        {
            return new DashboardVM()
            {
                User = Mapper.Map<UserVM>(_unitOfWork.GetRepo<User>().Get(user => user.Id == userId).Single()),
                ActorsCnt = _unitOfWork.GetRepo<Actor>().Get().Count(),
                CategoriesCnt = _unitOfWork.GetRepo<Category>().Get().Count(),
                FilmsCnt = _unitOfWork.GetRepo<Film>().Get().Count()
            };
        }
    }
}