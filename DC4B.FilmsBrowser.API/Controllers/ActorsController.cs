﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DC4B.FilmsBrowser.API.Filters;
using DC4B.FilmsBrowser.Common.ApiVM.Actor;
using DC4B.FilmsBrowser.Entities;
using DC4B.FilmsBrowser.Services.Abstract;
using AutoMapper;

namespace DC4B.FilmsBrowser.API.Controllers
{
    [ModelValidation]
   // [AuthorizeFor("Member")]
    [Authorize(Roles="Member")]
    [RoutePrefix("api/actors")]
    public class ActorsController : BaseCRUDController<Actor, ActorVM>
    {
        private readonly IActorService _actorService;
        public ActorsController(IActorService actorService) : base(actorService)
        {
            _actorService = actorService;
        }

        /// <summary>
        /// Overrided because i need use formatted date for other view model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override IHttpActionResult Get(int id)
        {
            var data = _actorService.GetById(id);
            if (data == null)
                return Error(HttpStatusCode.NotFound, "Actor not found");
            return Ok(Mapper.Map<ActorWithoutFilmsVM>(data));
        }
    }
}
