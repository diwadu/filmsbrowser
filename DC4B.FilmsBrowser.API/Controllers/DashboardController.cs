﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DC4B.FilmsBrowser.API.Filters;
using DC4B.FilmsBrowser.Services.Abstract;

namespace DC4B.FilmsBrowser.API.Controllers
{
    [Authorize(Roles = "Member")]
    [RoutePrefix("api/dashboard")]
    public class DashboardController : BaseApiController
    {
        private readonly IDashboardService _dashboardService;
        public DashboardController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
        }

        public IHttpActionResult Get()
        {
            return Ok(_dashboardService.GetData(GetUserId()));
        }

    }
}
