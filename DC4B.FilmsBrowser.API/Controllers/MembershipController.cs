﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DC4B.FilmsBrowser.API.Filters;
using DC4B.FilmsBrowser.Common.ApiVM.Membership;
using DC4B.FilmsBrowser.Services.Abstract;
using System.Web;

namespace DC4B.FilmsBrowser.API.Controllers
{
    [RoutePrefix("api")]
    public class MembershipController : BaseApiController
    {
        private readonly IMembershipService _membershipService;
        public MembershipController(IMembershipService membershipService)
        {
            _membershipService = membershipService;
        }

        [HttpPost, Route("login")]
        public IHttpActionResult Login([FromBody] LoginVM loginModel)
        {
            bool validLogin = _membershipService.VerifyLogin(loginModel.Email);
            if (!validLogin)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid login"));

            bool validPassword = _membershipService.VerifyPassword(loginModel.Email, loginModel.Password);
            if (!validPassword)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid password"));

            return Ok();
        }

        [Authorize(Roles ="Member, Admin")]
        [HttpGet, Route("isLoggedIn")]
        public IHttpActionResult IsLoggedIn()
        {
            return Ok();
        }

        [Authorize(Roles = "Member, Admin")]
        [HttpGet, Route("logout")]
        public IHttpActionResult Logout()
        {
            HttpContext.Current.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalBearer);
            return Ok();
        }
    }
}
