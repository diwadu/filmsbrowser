﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DC4B.FilmsBrowser.Infrastructure.SqlServer
{
    public interface ISqlRepository<T> where T : class
    {
        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null);
        T GetById(object id);
        void Insert(T entity);
        void InsertRange(IEnumerable<T> entities);
        void Update(T entity);
        void Delete(int id);
        void Delete(T entity);
    }
}
