'use strict';

/**
 * @ngdoc service
 * @name webClientApp.actorService
 * @description
 * # actorService
 * Service in the webClientApp.
 */
angular.module('webClientApp')
	.service('ActorService', function(BaseService) {

		angular.extend(this, BaseService);

		this.getUpdateMetadata = function(id) {
			return BaseService.http.get('actors/'+id);
		};

		/*this.get = function() {
			var deferred = $q.defer();

			$http({
				method: 'GET',
				url: appConfig.apiUrl + 'actors',
			}).success(function(resData, status, headers, config) {
				deferred.resolve(resData);
			}).error(function(resData, status, headers, config) {
				deferred.reject(resData);
			});

			return deferred.promise;
		};

		this.create = function(actor) {
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: appConfig.apiUrl + 'actors',
				data: actor,
			}).success(function(resData, status, headers, config) {
				deferred.resolve(resData);
			}).error(function(resData, status, headers, config) {
				deferred.reject(resData);
			});

			return deferred.promise;
		};

		

		this.update = function(id, actor) {
			var deferred = $q.defer();

			$http({
				method: 'PUT',
				url: appConfig.apiUrl + 'actors/'+id,
				data: actor,
			}).success(function(resData, status, headers, config) {
				deferred.resolve(resData);
			}).error(function(resData, status, headers, config) {
				deferred.reject(resData);
			});

			return deferred.promise;
		};
		*/

	});