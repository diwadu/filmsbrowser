'use strict';

/**
 * @ngdoc service
 * @name webClientApp.defaultInterceptor
 * @description
 * # defaultInterceptor
 * Service in the webClientApp.
 */
angular.module('webClientApp')
  .factory('DefaultInterceptor', function($q, $localStorage, $rootScope, $location, $window) {
    return {
      request: function(config) {
        config.headers['Content-Type'] = 'application/json';
        var authToken = $localStorage.authToken;
        if (authToken) {
          config.headers['Authorization'] = 'Bearer ' + authToken;
        }
        return config || $q.when(config);
      },
      requestError: function(rejection) {
        /*console.log('ICTR: Request Error');*/
        return $q.reject(rejection);
      },
      response: function(response) {
        switch (response.status) {
          case 0:
              break;
          case 200:
            /*console.log('ICTR: Ok');*/
            break;
          case 201:
            toastr.success('Created');
             //$window.history.back();
            break;
          case 204:
            toastr.success('Updated');
             //$window.history.back();
            break;
          default:
            break;
        }
        return response || $q.when(response);
      },
      responseError: function(rejection) {
        switch (rejection.status) {
          case -1:
          case 0:
            toastr.error('No connection');
            $location.path('/login');
            break;
          case 400:
            toastr.error('Bad request (400)');
            break;
          case 401:
            $rootScope.isLoggedIn = false;
            $location.path('/login');
             /*console.log('ICTR: Not authorized');*/
            break;
          case 403:
             $location.path('/forbidden');
            break;
          case 404:
            /*console.log('ICTR: Not found');*/
            break;
          case 409:
            /*console.log('ICTR: Conflict');*/
            break;
          case 500:
            /*console.log('ICTR: Internal server error');*/
            break;
          default:
            break;
        }
        return $q.reject(rejection);
      }
    }
  });
