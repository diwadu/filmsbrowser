'use strict';

/**
 * @ngdoc service
 * @name webClientApp.filmService
 * @description
 * # filmService
 * Service in the webClientApp.
 */
angular.module('webClientApp')
	.service('FilmService', function(BaseService) {
		angular.extend(this, BaseService);
	});