'use strict';

/**
 * @ngdoc service
 * @name webClientApp.meService
 * @description
 * # meService
 * Service in the webClientApp.
 */
angular.module('webClientApp')
    .service('MeService', function($http, $q, appConfig) {
        this.get = function() {
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: appConfig.apiUrl + 'initialView',
            }).success(function(resData, status, headers, config) {
                deferred.resolve(resData);
            }).error(function(resData, status, headers, config) {
                deferred.reject(resData);
            });

            return deferred.promise;
        };


        this.update = function(user) {
            var deferred = $q.defer();

            $http({
                method: 'PUT',
                url: appConfig.apiUrl + 'initialView',
                data: user,
            }).success(function(resData, status, headers, config) {
                deferred.resolve(resData);
            }).error(function(resData, status, headers, config) {
                deferred.reject(resData);
            });

            return deferred.promise;
        };
    });
