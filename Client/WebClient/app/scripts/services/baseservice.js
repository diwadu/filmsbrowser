'use strict';

/**
 * @ngdoc service
 * @name webClientApp.BaseService
 * @description
 * # BaseService
 * Service in the webClientApp.
 */
angular.module('webClientApp')
    .service('BaseService', function($q, $http, appConfig) {

        this.test = function() {
            console.log('test() from BaseService!');
        };

        this.http = {
            get: function(action) {

                var deferred = $q.defer();

                $http({
                    method: 'GET',
                    url: appConfig.apiUrl + action
                }).success(function(resData, status, headers, config) {
                    deferred.resolve(resData);
                }).error(function(resData, status, headers, config) {
                    deferred.reject(resData);
                });

                return deferred.promise;

            },

            post: function(action, data, headers = {}) {

                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: appConfig.apiUrl + action,
                    data: data,
                    headers: headers
                }).success(function(resData, status, headers, config) {
                    deferred.resolve(resData);
                }).error(function(resData, status, headers, config) {
                    deferred.reject(resData);
                });

                return deferred.promise;

            },

            put: function(action, data, headers = {}) {

                var deferred = $q.defer();

                $http({
                    method: 'PUT',
                    url: appConfig.apiUrl + action,
                    data: data,
                    headers: headers
                }).success(function(resData, status, headers, config) {
                    deferred.resolve(resData);
                }).error(function(resData, status, headers, config) {
                    deferred.reject(resData);
                });

                return deferred.promise;

            }
        }

        this.read = function(action) {
            return this.http.get(action);
        }

        this.create = function(action, data) {
            return this.http.post(action, data);
        }

        this.update = function(action, data) {
            return this.http.put(action, data);
        }
    });
