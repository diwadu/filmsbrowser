'use strict';

/**
 * @ngdoc filter
 * @name webClientApp.filter:firstPathSegment
 * @function
 * @description
 * # firstPathSegment
 * Filter in the webClientApp.
 */
angular.module('webClientApp')
	.filter('firstPathSegment', function() {
		// path example: /route1/2/route2
		return function(input) {
			return input.split('/')[1];
		}
	});