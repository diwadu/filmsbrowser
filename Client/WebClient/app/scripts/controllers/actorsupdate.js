'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:ActorsupdateCtrl
 * @description
 * # ActorsupdateCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
	.controller('ActorsupdateCtrl', function($scope, $routeParams, ActorService, $window) {
		var init = function() {
			ActorService.getUpdateMetadata($routeParams.id).then(
				function(result) {
					$scope.actor = result;
					console.log($scope.film);
				},
				function(error) {
					console.log(error);
				}
			);
		}();

		$scope.update = function(actor){
			console.log(actor);
			ActorService.update('actors/' + $routeParams.id, actor).then(
				function(result) {
					console.log('Done');
					$window.history.back();
				},
				function(error) {
					console.log(error);
				}
			);
		};
	});