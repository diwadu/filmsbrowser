'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
  .controller('LoginCtrl', function($scope, $location, AuthService) {


    $scope.login = function(email, pwd) {

      AuthService.login(email, pwd).then(
        function(result) {
          AuthService.setUserData(result);
          $location.path('/dashboard');
        },
        function(error) {
          console.log(error);
          AuthService.resetUserData();
        }
      );

    };
  });