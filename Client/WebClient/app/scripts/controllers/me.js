'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:MeCtrl
 * @description
 * # MeCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
    .controller('MeCtrl', function($scope, MeService) {

		$scope.myForms = {};
		
        var init = function() {
        	 
            MeService.get().then(
                function(result) {
                    $scope.user = result.User;
                },
                function(error) {
                    console.log('Error:', error);
                });
        }();

        $scope.update = function(user) {
            MeService.update(user).then(
                function(result) {
                    console.log('Updated');
                    $scope.myForms.userForm.$setPristine();
                },
                function(error) {
                    console.log('Error:', error);
                });
        };
    });
