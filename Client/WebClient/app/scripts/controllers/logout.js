'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
  .controller('LogoutCtrl', function ($scope, AuthService, $location) {


     $scope.logout = function(){
        AuthService.logout().then(
        function(result) {
          AuthService.resetUserData(result);
          $location.path('/login');
        }, function(error) {
          console.log(error);
        }
      );
    };
  });
