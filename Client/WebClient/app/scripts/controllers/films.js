'use strict';

/**
 * @ngdoc function
 * @name webClientApp.controller:FilmsCtrl
 * @description
 * # FilmsCtrl
 * Controller of the webClientApp
 */
angular.module('webClientApp')
	.controller('FilmsCtrl', function($scope, FilmService) {

		var init = function() {
			FilmService.read('films').then(
				function(result) {
					$scope.films = result;
				},
				function(error) {
					console.log('Error:', error);
				});
		}();

	});