'use strict';

describe('Service: MeService', function () {

  // load the service's module
  beforeEach(module('webClientApp'));

  // instantiate service
  var MeService;
  beforeEach(inject(function (_MeService_) {
    MeService = _MeService_;
  }));

  it('should do something', function () {
    expect(!!MeService).toBe(true);
  });

});
