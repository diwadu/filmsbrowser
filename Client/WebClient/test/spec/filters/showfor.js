'use strict';

describe('Filter: showFor', function () {

  // load the filter's module
  beforeEach(module('webClientApp'));

  // initialize a new instance of the filter before each test
  var showFor;
  beforeEach(inject(function ($filter) {
    showFor = $filter('showFor');
  }));

  it('should return the input prefixed with "showFor filter:"', function () {
    var text = 'angularjs';
    expect(showFor(text)).toBe('showFor filter: ' + text);
  });

});
