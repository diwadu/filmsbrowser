'use strict';

describe('Controller: FilmsupdateCtrl', function () {

  // load the controller's module
  beforeEach(module('webClientApp'));

  var FilmsupdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FilmsupdateCtrl = $controller('FilmsupdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
