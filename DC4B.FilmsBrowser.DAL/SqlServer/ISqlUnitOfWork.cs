﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC4B.FilmsBrowser.DAL.SqlServer
{
    public interface ISqlUnitOfWork : IDisposable
    {
        ISqlRepository<TEntity> GetRepo<TEntity>() where TEntity : class;
        object Commit();
        //DbContext DataContext { get; }
    }
}
