using System;
using System.Data.Entity;

namespace DC4B.FilmsBrowser.DAL.SqlServer
{
    public class SqlUnitOfWork : ISqlUnitOfWork
    {
        private readonly DbContext _context;
        private bool _disposed;
        //public DbContext DataContext { get { return _context; } }

        public SqlUnitOfWork(DbContext context)
        {
            _context = context;
        }

        public object Commit()
        {
            return _context.SaveChanges();
        }

        public ISqlRepository<TEntity> GetRepo<TEntity>() where TEntity : class
        {
            return new SqlRepository<TEntity>(_context);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}