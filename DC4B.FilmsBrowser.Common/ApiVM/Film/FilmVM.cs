﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DC4B.FilmsBrowser.Common.ApiVM.Actor;

namespace DC4B.FilmsBrowser.Common.ApiVM.Film
{
    public class FilmVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        public DateTime PremiereDate { get; set; }

        [Required(ErrorMessage = "Required")]
        public int CategoryId { get; set; }

        //public IEnumerable<ActorVM> ActorsList { get; set; }
        public int[] Actors { get; set; }
    }
}
