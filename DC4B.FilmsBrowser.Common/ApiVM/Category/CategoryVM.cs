﻿using System.ComponentModel.DataAnnotations;

namespace DC4B.FilmsBrowser.Common.ApiVM.Category
{
    public class CategoryVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Field 'Name' is required", AllowEmptyStrings = false)]
        public string Name { get; set; }
    }
}
