﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC4B.FilmsBrowser.Entities
{
    public class DataContext : DbContext
    {
        private const string DbName = "FilmsBrowser";

        public DataContext() : base(DbName)
        {
            //Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Migrations.Configuration>(DbName));
        }

        public DbSet<Film> Films { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //one to many
            modelBuilder.Entity<Category>().HasMany<Film>(f => f.Films)
                .WithRequired(fc => fc.Category)
                .HasForeignKey(f => f.CategoryId);

            //many to many with new table
            modelBuilder.Entity<Film>()
                .HasMany(f => f.Actors)
                .WithMany(a => a.Films)
                .Map(m =>
                {
                    m.MapLeftKey("FilmId");
                    m.MapRightKey("ActorId");
                    m.ToTable("FilmActors");
                });

            //m:n users and roles
            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                    m.ToTable("UsersRoles");
                });
        }
    }
}