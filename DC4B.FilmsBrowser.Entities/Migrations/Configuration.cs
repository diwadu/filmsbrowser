using System.Collections.Generic;
using System.Globalization;
using DC4B.FilmsBrowser.Entities.Helpers;

namespace DC4B.FilmsBrowser.Entities.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DC4B.FilmsBrowser.Entities.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DC4B.FilmsBrowser.Entities.DataContext context)
        {

            #region users roles
            var usr1 = new User
            {
                Email = "johndoe@gmail.com",
                FirstName = "John",
                LastName = "Doe",
                Password = PasswordManager.Hash("test")
            };
            var usr2 = new User
            {
                Email = "janedoe@gmail.com",
                FirstName = "Jane",
                LastName = "Doe",
                Password = PasswordManager.Hash("test")
            };

            var usr3 = new User
            {
                Email = "johnsmith@admin.com",
                FirstName = "John",
                LastName = "Smith",
                Password = PasswordManager.Hash("test")
            };

            var r1 = new Role
            {
                Id = 1,
                Name = "Member",
                Users = new[] { usr1, usr2 }
            };
            var r2 = new Role
            {
                Id = 2,
                Name = "Admin",
                Users = new[] { usr3 }
            };

            #endregion
            //ctgs
            var ctg1 = new Category { Name = "Comedy" };
            var ctg2 = new Category { Name = "Drama" };
            var ctg3 = new Category { Name = "Scifi" };

            //actors
            var a1 = new Actor {FirstName = "Arnold", LastName = "Schwartzeneger", BirthDate = DateTime.Now.AddYears(-75) };
            var a2 = new Actor {FirstName = "Keanu", LastName = "Reews",BirthDate = DateTime.Now.AddYears(-45)};
            var a3 = new Actor {FirstName = "John",LastName = "Travolta",BirthDate = DateTime.Now.AddYears(-60)};
            var a4 = new Actor {FirstName = "Bruce",LastName = "Willis",BirthDate = DateTime.Now.AddYears(-65)};
            var a5 = new Actor {FirstName = "Marylin",LastName = "Monroe",BirthDate = DateTime.Now.AddYears(-90)};
            var a6 = new Actor {FirstName = "Samuel", LastName = "Jackson", BirthDate = DateTime.Now.AddYears(-70)};

            //films
            var f1 = new Film {Name = "Matrix",  PremiereDate = DateTime.Now.AddYears(-10),Category = ctg3, Actors = new[] {a2}};
            var f2 = new Film {Name = "Terminator", PremiereDate = DateTime.Now.AddYears(-20), Category = ctg2, Actors = new[] {a1}};
            var f3 = new Film {Name = "Pulp Fiction", PremiereDate = DateTime.Now.AddYears(-5), Category = ctg3, Actors = new[] {a3, a6}};
            var f4 = new Film {Name = "Mens preffer blondes", PremiereDate = DateTime.Now.AddYears(-50),Category = ctg1, Actors = new[] {a5}};


            using (var ctx = new DataContext())
            {
                ctx.Categories.AddRange(new[] { ctg1, ctg2, ctg3 });
                ctx.Actors.AddRange(new[] { a1, a2, a3, a4, a5, a6 });
                ctx.Films.AddRange(new[] {f1, f2, f3, f4});
                ctx.Users.AddRange(new[] {usr1, usr2, usr3});
                ctx.Roles.AddRange(new[] {r1, r2});

                //ctx.SaveChanges();
            }
        }
    }
}
